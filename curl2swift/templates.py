TEST_TEMPLATE = """
    func test<REQUEST_NAME>Request() {
        let expectation = XCTestExpectation(description: "waiting for reponse")
        <REQUEST_NAME>Request()
            .set(.baseURL("<URL>"))
            .set(.path("<PATH>"))
            <HEADER_SETTERS>
            <BODY_PARAM_SETTERS>
            .makeRxRequest()
            .mapTo(<REQUEST_NAME>Request.Response.self)
            .do(onSuccess: { _ in expectation.fulfill() },
                onError: { _ in XCTFail("The request should succeed") })
            .discardableSubscribe()
        wait(for: [expectation], timeout: 10)
    }
"""

QUERY_PARAM_SETTER = """
    @discardableResult
    func setQueryParam(_ key: QueryParam, _ value: String) -> Self {
        queryParams[key.rawValue] = value
        return self
    }
"""

PATH_PARAM_SETTER = \
    """@discardableResult
    func setPathParameter(_ param: PathParameter, _ value: String) -> Self {
        let placeholder = "{" + param.rawValue + "}"
        path = path.replacingOccurrences(of: placeholder, with: value)
        return self
    }"""

HEADER_PARAM_SETTER = \
    """@discardableResult
    func setHeader(_ key: Header, _ value: String) -> Self {
        headers[key.rawValue] = value
        return self
    }
    """

BODY_PARAM_SETTER = \
    """@discardableResult
    func setBodyParameter(_ key: BodyParameter, _ value: String) -> Self {
        params[key.rawValue] = value
        return self
    }"""

REQUEST_TEMPLATE = """
/// <DESC>
class <REQUEST_NAME>Request: RequestSpecBuilder {

    <RESPONSE>

    enum QueryParam: String {
        <QUERY_PARAMS>
    }

    enum PathParameter: String {
        <PATH_PARAMS>
    }

    enum Header: String {
        <HEADERS>
    }

    enum BodyParameter: String {
        <BODY_PARAMS>
    }

    required init(baseURL: String = "",
                  path: String = "",
                  queryParams: [String: String] = [:],
                  method: HTTPMethod = .get,
                  headers: [String: String] = [:],
                  params: [String: Any] = [:]) {
        super.init(baseURL: baseURL, path: path, method: method, headers: headers, params: params)
        set(.path("<PATH>"))
        <QUERY_PARAMS_INIT>
        set(.method(<METHOD>))
    }
}

extension <REQUEST_NAME>Request {

    <QUERY_PARAM_SETTER>

    <PATH_PARAM_SETTER>

    <HEADER_PARAM_SETTER>

    <BODY_PARAM_SETTER>
}
"""

CODABLE_TEMPLATE = """
    struct <MODEL_NAME>: Codable {
        <PROPERTIES>

        enum CodingKeys: String, CodingKey {
            <CODING_KEYS>
        }
    }
"""